# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2020 wilkie

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from occam.log              import Log
from occam.manager          import uses

from occam.commands.manager import command, option, argument

from occam.search.manager   import SearchManager
from occam.objects.manager  import ObjectManager

@command('search', 'set',
         category      = "Search",
         documentation = "Updates an index entry.")
@argument("object", type = "object")
@uses(SearchManager)
@uses(ObjectManager)
class SearchSetCommand:
  def do(self):
    """ This command updates the index entry pertaining to the given object.
    """

    obj = self.objects.resolve(self.options.object, person = self.person)
    if obj is None:
      # Cannot find object
      Log.error(key="occam.objects.errors.specifiedObjectNotFound", id=self.options.object.id)
      return 1

    if self.search.set(obj):
      return 0

    return 1
