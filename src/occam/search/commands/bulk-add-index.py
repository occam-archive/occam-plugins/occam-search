#This command adds items to an ElasticSearch index.

# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2019 wilkie

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from occam.log              import Log
from occam.manager          import uses

from occam.commands.manager import command, option

from occam.search.manager   import SearchManager

#from occam_es import ElasticSearchHelpers

@command('search', 'bulk-add-index',
         category      = "Object Discovery",
         documentation = "Add objects to the ElasticSearch index so that they can be searchable later."
)
@option("-j", action = "store", dest = "objects_json", help="bulk add objects from a given json file to the index")

@uses(SearchManager)
class BulkAddCommand:
  def do(self):
    print("BulkAddCommand called")
    print(self)
    print(self.options.objects_json)
    self.search.bulk_add(self.options.objects_json)
    return 0