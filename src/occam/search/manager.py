# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2020 wilkie

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import json

# Manager imports
from occam.log     import loggable
from occam.manager import manager, uses

# The ObjectManager, for querying object metadata
from occam.objects.manager import ObjectManager

@loggable
@uses(ObjectManager)
@manager("search")
class SearchManager:
  """ Represents all indexed search operations.
  """

  handlers = {}
  instantiated = {}

  def __init__(self):
    """ Initializes the manager.
    """

    # Import plugins
    import occam.search.plugins.elasticsearch

  @staticmethod
  def register(indexName, handlerClass):
    """ Adds a new search index backend.

    Arguments:
      handlerClass (object): The handler that implements the search plugin interface.
    """

    SearchManager.handlers[indexName] = {'class': handlerClass}

  def handlerFor(self, indexName):
    """ Returns an instance of a handler for the given name.
    """

    if not indexName in SearchManager.handlers:
      SearchManager.Log.error("search backend %s not known" % (indexName))
      return None

    # Instantiate a search backend if we haven't seen it before
    if not indexName in SearchManager.instantiated:
      # Pull the configuration (from the 'search' subpath and keyed by the name)
      subConfig = self.configurationFor(indexName)

      # Create a driver instance
      instance = SearchManager.handlers[indexName]['class'](subConfig)

      # If there is a problem detecting the backend, cancel
      # This will set the value in the instantiations to None
      if hasattr(instance, 'detect') and callable(instance.detect) and not instance.detect():
        instance = None

      # Set the instance
      SearchManager.instantiated[indexName] = instance

    if SearchManager.instantiated[indexName] is None:
      # The driver could not be initialized
      return None

    return SearchManager.instantiated[indexName]

  def configurationFor(self, indexName):
    """ Returns the configuration for the given plugin.

    Returns the configuration for the given search plugin that is found within
    the occam configuration (config.yml) under the "search" section under the
    "plugins" section and then under the given search backend name.
    """

    config = self.configuration
    pluginConfig = config.get('plugins', {})
    subConfig = pluginConfig.get(indexName, {})

    return subConfig

  def initialize(self):
    """ Initializes the search index plugins.

    Returns:
      bool: Returns True when the component is initialized.
    """

    # Go through each plugin and initialize it.
    for name in SearchManager.handlers:
      self.handlerFor(name).initialize()

    return True

  def clear(self):
    """ Clears the object index.

    Returns:
      bool: True when the index is confirmed to not exist.
    """

    return self.handlerFor("elasticsearch").clear()

  def fullSearch(self, query=None,
                 id=None, uid=None, name=None, object_type=None, subtype=None, environments=None,
                 excludeEnvironments=None, architectures=None, excludeArchitectures=None,
                 provides=None, viewsType=None, viewsSubtype=None, identity=None,
                 excludeTypes=None, tags=None, canBeReadBy=None):
    """ Returns a full search response.
    """

    # There are certain situations where it does not make sense to do an indexed
    # search. Here, we fall back to the normal everyday search.
    if query is None or id:
      # Just return the normal search results to get 'everything' when no
      # query string is given.

      # Or when 'id' is given. In that case, we are searching for a single
      # document... just use the normal search.
      return self.objects.fullSearch(id = id,
                                     uid = uid,
                                     name = name,
                                     object_type = object_type,
                                     subtype = subtype,
                                     environments = environments,
                                     architectures = architectures,
                                     excludeEnvironments = excludeEnvironments,
                                     excludeArchitectures = excludeArchitectures,
                                     provides = provides,
                                     viewsType = viewsType,
                                     viewsSubtype = viewsSubtype,
                                     identity = identity,
                                     excludeTypes = excludeTypes,
                                     tags = tags,
                                     canBeReadBy = canBeReadBy)

    ret = self.handlerFor("elasticsearch").fullSearch(query = query,
                                                      name = name,
                                                      uid = uid,
                                                      object_type = object_type,
                                                      subtype = subtype,
                                                      environments = environments,
                                                      architectures = architectures,
                                                      excludeEnvironments = excludeEnvironments,
                                                      excludeArchitectures = excludeArchitectures,
                                                      provides = provides,
                                                      viewsType = viewsType,
                                                      viewsSubtype = viewsSubtype,
                                                      identity = identity,
                                                      excludeTypes = excludeTypes,
                                                      tags = tags,
                                                      canBeReadBy = canBeReadBy)

    if ret is False:
      # Ok, our indexer failed, fall back to a normal search
      ret = self.objects.fullSearch(name = name,
                                    uid = uid,
                                    object_type = object_type,
                                    subtype = subtype,
                                    environments = environments,
                                    architectures = architectures,
                                    excludeEnvironments = excludeEnvironments,
                                    excludeArchitectures = excludeArchitectures,
                                    provides = provides,
                                    viewsType = viewsType,
                                    viewsSubtype = viewsSubtype,
                                    identity = identity,
                                    excludeTypes = excludeTypes,
                                    tags = tags,
                                    canBeReadBy = canBeReadBy)

    return ret

  def set(self, obj, objInfo=None, person=None):
    """ Updates the index with the information represented by the given object.

    Arguments:
      obj (Object): The Occam object to add to the index.
      objInfo (ObjectInfo): Optionally, the object info to use.
      person (Person): The actor making this request.

    Returns:
      bool: True when the document is successfully updated or created.
    """

    objInfo = objInfo or self.objects.infoFor(obj)

    # Normalize subtype list
    subtypes = objInfo.get('subtype', [])
    if not isinstance(subtypes, list):
      subtypes = [subtypes]

    # Normalize the description field
    description = objInfo.get('description', '')
    if isinstance(description, dict):
      if 'file' in description:
        # Retrieve the file containing the description
        data = self.objects.retrieveFileFrom(obj, path = description['file'],
                                                  includeResources = True,
                                                  person = person)
        description = data.read().decode('utf-8')

    normalized = {
      'identity': obj.identity,
      'name': objInfo.get('name'),
      'type': objInfo.get('type'),
      'summary': objInfo.get('summary', ''),
      'description': description,
      'tags': objInfo.get('tags', []),
      'architecture': objInfo.get('architecture', ''),
      'environment': objInfo.get('environment', ''),
      'subtype': subtypes,
      'organization': objInfo.get('organization', ''),
      'revision': obj.revision,
      'images': objInfo.get('images', []),
      'file': objInfo.get('file', ''),
      'uid': obj.uid,
      'id': obj.id
    }

    return self.handlerFor("elasticsearch").set(normalized)

def indexer(name):
  """ This decorator will register the given class as a search backend.
  """

  def register_index(cls):
    SearchManager.register(name, cls)
    cls = loggable("SearchManager")(cls)

    def init(self, subConfig):
      self.configuration = subConfig

    cls.__init__ = init

    return cls

  return register_index

class SearchIndexPlugin:
  """ The interface for a search index plugin.
  """

  def detect(self):
    """ Detects the presence of the search index service.

    This function is meant to detect whether or not the intended service is
    installed or otherwise available on the current system.

    The plugin will be automatically disabled when the service is not detected.

    Returns:
      bool: Returns True when the service, as configured, exists.
    """

    return False

  def initialize(self):
    """ Performs initial setup of the service.
    """

  def clear(self):
    """ Clears all entries in the index.

    Returns:
      bool: True when the index is confirmed to not exist.
    """

    return False

  def fullSearch(self, query=None, id=None, uid=None, name=None, object_type=None,
                       subtype=None, environments=None, excludeEnvironments=None,
                       architectures=None, excludeArchitectures=None, provides=None,
                       viewsType=None, viewsSubtype=None, identity=None,
                       excludeTypes=None, tags=None, canBeReadBy=None):
    """ Returns a full search response.
    """

    return False

  def set(self, objInfo):
    """ Updates the index with the information represented by the given object.

    Arguments:
      objInfo (ObjectInfo): The Occam object metadata we are adding to the index.

    Returns:
      bool: True when the document is successfully updated or created.
    """

    return False
