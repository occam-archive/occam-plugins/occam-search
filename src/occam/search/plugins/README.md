# Search plugin interface

This directory contains a set of plugins that enable different indexing
technologies. When you want to support a new search service, you can implement
a plugin to provide a bridge from the Search Component to that service.
