# Elasticsearch plugin

# Normal imports
from occam.search.manager import indexer, SearchIndexPlugin

# ElasticSearch imports
from elasticsearch import Elasticsearch, helpers, exceptions
from elasticsearch_dsl import Index, Search, FacetedSearch, TermsFacet, Document
from elasticsearch_dsl import Keyword, Text
from elasticsearch_dsl import analyzer, tokenizer
from elasticsearch_dsl.query import MultiMatch

# An N-Gram tokenizer for simple text fields (name, etc)
ANALYZER = analyzer(
  "query_analyzer", tokenizer = tokenizer("gram", "ngram", min_gram=3, max_gram=3),
  filter = ['lowercase']
)

# An Elasticsearch DSL representation of an 'object'
class ObjectDocument(Document):
  """ The DSL representation of the object document in the index.
  """

  name = Text(fields = {'ngram': Text(analyzer = ANALYZER), 'keyword': Keyword()})
  object_type = Keyword()
  subtype = Keyword(multi = True)
  description = Text()
  organizationn = Text(fields = {'ngram': Text(analyzer = ANALYZER), 'keyword': Keyword()})
  revision = Keyword()
  images = Text()
  summary = Text()
  file = Text()
  uid = Keyword()
  id = Keyword()
  identity = Keyword()
  environment = Keyword()
  architecture = Keyword()
  tags = Keyword(multi = True)

# The Elasticsearch DSL faceted search interface
class ObjectSearch(FacetedSearch):
  """ A DSL for searching for objects.
  """

  doc_types = [ObjectDocument]

  # This searches the 'description' at a default score, 'tags' at 3x score
  # and 'name' at 5x score (partial match via ngram at 3x).
  fields = ['tags^3', 'name.keyword^5', 'name.ngram^3', 'description']

  # Each facet we want to aggregate
  facets = {
    'tags': TermsFacet(field = 'tags.keyword'),
    'object_type': TermsFacet(field = 'object_type.keyword'),
    'environment': TermsFacet(field = 'environment.keyword'),
    'architecture': TermsFacet(field = 'architecture.keyword'),
    'subtype': TermsFacet(field = 'subtype.keyword'),
  }

  def __init__(self, query = None, using = None, index = None, exclude = None, **kwargs):
    self.using = using
    self.index = index
    self.exclude = exclude

    super(ObjectSearch, self).__init__(query, **kwargs)

  def search(self):
    search = super().search()
    if self.exclude:
      for k, v in self.exclude.items():
        search = search.exclude('terms', **({k: v}))
    return search

@indexer('elasticsearch')
class ElasticsearchPlugin(SearchIndexPlugin):
  def connect(self):
    """ Connects to Elasticsearch, if not already connected.
    """

    if not hasattr(self, 'es'):
      # Gather the configuration (from the occam config under the 'search' header)
      host = self.configuration.get('host', 'localhost')
      port = self.configuration.get('port', 9200)
      self._prefix = self.configuration.get('prefix', 'occam')
      self._index = self._prefix + "-objects"

      # Connects to an elastic search service
      self.es = Elasticsearch(host=host, port=port)

  def detect(self):
    """ Detects the presence of Elasticsearch.

    Returns:
      bool: Returns True when Elasticsearch, as configured, exists.
    """

    return True

  def initialize(self):
    """ Initializes the Elasticsearch index.

    Returns:
      bool: Returns True when Elasticsearch successfully initialized the index.
    """

    self.connect()

    # Ensure the index exists (and is closed)
    index = Index(self._index)

    # Ensure, if it exists, that it is closed
    if self.es.indices.exists(index = self._index):
      index.close(using = self.es)

    # Initialize the object document type in this index
    ObjectDocument.init(index = self._index, using = self.es)

    # Re-open the index
    index.open(using = self.es)

  def clear(self):
    """ Clears the Elasticsearch object index.

    Returns:
      bool: True when the index is confirmed to not exist.
    """

    self.connect()

    # Attempt to destroy the index
    try:
      self.es.indices.delete(index = self._index)
    except exceptions.NotFoundError:
      # This is ok, the index does not exist anyway
      pass

    # Returns true to confirm index is deleted
    return not self.es.indices.exists(index = self._index)

  def fullSearch(self, query=None, id=None, uid=None, name=None, object_type=None,
                       subtype=None, environments=None, excludeEnvironments=None,
                       architectures=None, excludeArchitectures=None, provides=None,
                       viewsType=None, viewsSubtype=None, identity=None,
                       excludeTypes=None, tags=None, canBeReadBy=None):
    """ Returns a full search response.
    """

    self.connect()

    # Apply facet filters
    filters = {
      'environment': environments or [],
      'architecture': architectures or [],
      'tags': tags or []
    }

    if object_type:
      filters['object_type'] = [object_type]

    if subtype:
      filters['subtype'] = subtype or []

    exclude = {
      'environment': excludeEnvironments or [],
      'architecture': excludeArchitectures or [],
      'object_type': excludeTypes or [],
    }

    # Craft the appropriate query via the DSL
    search = ObjectSearch(query, using = self.es,
                                 index = self._index,
                                 filters = filters,
                                 exclude = exclude)

    # Perform the query
    try:
      response = search.execute()
    except exceptions.NotFoundError:
      return False

    ret = {
      'tags': [],
      'types': [],
      'environments': [],
      'architectures': []
    }

    # Parse out facets
    for (tag, count, selected) in response.facets.tags:
      if tag == "":
        # No tag
        continue
      ret['tags'].append(tag)

    for (tag, count, selected) in response.facets.object_type:
      if tag == "":
        # No tag
        continue
      ret['types'].append(tag)

    for (tag, count, selected) in response.facets.environment:
      if tag == "":
        # No environment
        continue
      ret['environments'].append(tag)

    for (tag, count, selected) in response.facets.architecture:
      if tag == "":
        # No architecture
        continue
      ret['architectures'].append(tag)

    ret['num_types'] = len(ret['types'])
    ret['num_environments'] = len(ret['environments'])
    ret['num_architectures'] = len(ret['architectures'])
    ret['num_tags'] = len(ret['tags'])

    # Get metadata
    ret['count'] = response.hits.total.value

    # Massage the results into ObjectRecord (with score)
    from occam.objects.records.object import ObjectRecord
    response = map(lambda x: ObjectRecord({
      'id': x.meta.id,
      'uid': x.uid,
      'name': x.name,
      'identity_uri': x.identity,
      'summary': x.summary,
      'description': x.description,
      'object_type': x.object_type,
      'tags': x.tags if hasattr(x, 'tags') else [],
      'architecture': x.architecture,
      'environment': x.environment,
      'file': x.file,
      'images': x.images,
      'revision': x.revision,
      'organization': x.organization,
      'subtype': x.subtype if hasattr(x, 'subtype') else [],
      'score': x.meta.score
    }), response)

    ret['objects'] = list(response)

    return ret

  def set(self, objInfo):
    """ Updates the index with the information represented by the given object.

    Arguments:
      objInfo (ObjectInfo): The Occam object metadata we are adding to the index.

    Returns:
      bool: True when the document is successfully updated or created.
    """

    self.connect()

    index = Index(self._index)
    index.document(ObjectDocument)

    document = ObjectDocument(identity = objInfo.get('identity'),
                              name = objInfo.get('name'),
                              object_type = objInfo.get('type'),
                              summary = objInfo.get('summary', ''),
                              description = objInfo.get('description', ''),
                              tags = objInfo.get('tags', []),
                              architecture = objInfo.get('architecture', ''), 
                              environment = objInfo.get('environment', ''),
                              subtype = objInfo.get('subtype', []),
                              organization = objInfo.get('organization', ''),
                              revision = objInfo.get('revision'),
                              images = objInfo.get('images', []),
                              file = objInfo.get('file', ''),
                              uid = objInfo.get('uid'),
                              meta = {
                                'id': objInfo.get('id')
                              })

    response = document.save(using = self.es, skip_empty = False)
    return response == "updated" or response == "created"
